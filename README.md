# LifeMon

## Server AWS IoT

```
cd server-awsiot
npm install
nodemon server.js
```

## Client

```
// Update socket-io to match your server ip:port in file ./src/components/Graph.vue

// Then
npm run serve

// Then open url: http://{ip}:{port}
```
