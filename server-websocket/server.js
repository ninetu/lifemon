const ECG = require('./ecg.js');
const ecg = new ECG();
const express = require('express');
const app = express();
const server = app.listen(3001, function() {
    console.log('server running on port 3001');
});

const frequency = 100;

const io = require('socket.io')(server);
io.on('connection', function(socket) {
    console.log(socket.id)
    socket.on('SEND_MESSAGE', function(data) {
        io.emit('MESSAGE', data)
    });
});

setInterval(function() {
    let val = ecg.tick()
    io.emit('MESSAGE', val[1]);
}, frequency);
