'use strict';
const PUBLISH_TO_SERVER = true;

console.log('Starting server.js')
const clientId = 'van1';
const util = require('./util.js');
const ECG = require('./ecg.js');
const ecg = new ECG();
const geoip = require('geoip-lite');

let awsIot = require('aws-iot-device-sdk');
let device = awsIot.device({
    keyPath: 'b1e92ff73b-private.pem.key',
    certPath: 'b1e92ff73b-certificate.pem.crt',
    caPath: 'AmazonRootCA1.pem',
    clientId: clientId,
    host: 'a1q7lheknc0kk2-ats.iot.ap-northeast-1.amazonaws.com'
});
device
    .on('connect', function () {
        console.log('connect');
    });
device
    .on('close', function () {
        console.log('close');
    });
device
    .on('reconnect', function () {
        console.log('reconnect');
    });
device
    .on('offline', function () {
        console.log('offline');
    });
device
    .on('error', function (error) {
        console.log('error', error);
    });
device
    .on('message', function (topic, payload) {
        console.log('message', topic, payload.toString());
    });

function publishEcg() {
    let data = {
        // ts: Date.now(),
        v: ecg.tick()[1]
    }
    if (PUBLISH_TO_SERVER) {
        device.publish('ecg', JSON.stringify(data));
    }
    console.log(data);
}
function publishHeartrate() {
    let data = {
        // ts: Date.now(),
        v: util.randomInt(70, 80),
    }
    if (PUBLISH_TO_SERVER) {
        device.publish('heartrate', JSON.stringify(data));
    }
    console.log(data);
}

function publishLocation() {
    let publicIp = require('public-ip');
    publicIp.v4()
        .then(ip => {
            let data = geoip.lookup(ip);
            data.ts = Date.now();
            if (PUBLISH_TO_SERVER) {
                device.publish('location', JSON.stringify(data));
            }
            console.log(data);
        })
        .catch(err => {
        })
}

publishEcg();
// publishHeartrate();
// publishLocation();
setInterval(publishEcg, 20); // send sample data every 500ms (but if demo must use 50ms)
// setInterval(publishHeartrate, 1000);
// setInterval(publishLocation, 60000);
