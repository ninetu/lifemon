'use strict';
const frequency = 1000;

const topic = 'heartrate';
console.log('Starting server.js')
const clientId = 'van1';
const util = require('./util.js');
const ECG = require('./ecg.js');
const ecg = new ECG();
let awsIot = require('aws-iot-device-sdk');
let device = awsIot.device({
    keyPath: 'b1e92ff73b-private.pem.key',
    certPath: 'b1e92ff73b-certificate.pem.crt',
    caPath: 'AmazonRootCA1.pem',
    clientId: clientId,
    host: 'a1q7lheknc0kk2-ats.iot.ap-northeast-1.amazonaws.com'
});
device
    .on('connect', function () {
        console.log('connect');
    });
device
    .on('close', function () {
        console.log('close');
    });
device
    .on('reconnect', function () {
        console.log('reconnect');
    });
device
    .on('offline', function () {
        console.log('offline');
    });
device
    .on('error', function (error) {
        console.log('error', error);
    });
device
    .on('message', function (topic, payload) {
        console.log('message', topic, payload.toString());
    });

let count = 0;
let timeout = setInterval(function () {
    count++;

    let data = {
        ts: Date.now(),
        v: util.randomInt(70, 80),
    }
    device.publish('heartrate', JSON.stringify(data));
    console.log(data);
}, frequency); // clip to minimum